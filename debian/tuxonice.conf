# Configuration file for TuxOnIce

# This shell snippet provides a way to configure both suspend & resume,
# by modifying TUXONICE_USERUI_PROGRAM variable or any TuxOnIce sysfs settings.

# TUXONICE_USERUI_PROGRAM is already set to enable default user interface,
# i.e. the text one, so no need to touch it if it's what you want.
# After this file is sourced, only user_interface/program is written with the
# content of TUXONICE_USERUI_PROGRAM variable.

# Current working directory is /sys/power/tuxonice
# All settings are documented in Documentation/power/tuxonice.txt
# (in linux source tree with TOI patch).

# /bin/sh is not running with -e flags so untested failed commands don't exit.

# First parameter can be: resume, hibernate or hybrid-sleep
# The following construct can be used for action-specific configuration:
if [ "$1" = resume ]; then
  # Resume configuration is only meaningful from an initrd.

  : # (nothing to do for me; TOI automatically find my swap partition)

else
  # Suspend will happen even if you exit with non-zero status.
  # This is not a place to inhibit suspend.

  # My swap partition is usually disabled, so activate it automatically
  # for hibernation. It's also possible to configure file allocator here.
  echo /dev/mapper/swap > swap/swapfilename

  # Not yet the default at the time of writing:
  echo lz4 > compression/algorithm

  echo 50 > compression/expected_compression

  # Typing R during suspend to reboot leaves reboot=1 after reboot.
  # Maybe you never want to reboot by default:
  echo 0 > reboot

  echo 1 > user_interface/enable_escape
  echo 1 > user_interface/default_console_level

fi

# Use the following line to enable fbsplash interface:
TUXONICE_USERUI_PROGRAM="$TUXONICE_USERUI_PROGRAM -f"
# You can also unset it (or play with user_interface/enabled) to disable
# user interface.
